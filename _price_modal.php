<div id="testModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" cg-busy="pricePromise" ng-cloak="">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color:#3F5B24;">&nbsp; Title</h4>
            </div>
            <div class="modal-body">
                Modal Body
                <div ng-repeat="wholesale in prices.wholesale_metrics">
                    <span style="font-size: 1.2em"> Metric: {{wholesale.metric}}</span>
                    <span class="label label-danger" style="font-size: 8px">wholesale</span>
                    <table class="table table-bordered table-condensed table-striped table-hover">
                        <tr>
                            <th>Market</th>
                            <th>Price</th>
                        </tr>
                        <tr ng-repeat="market in wholesale.markets">
                            <td>{{market.market_name}}</td>
                            <td>{{market.price| number:2}}</td>
                        </tr>

                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>