
<?php
include 'connection.php';
conT();

$categoryId = $_GET['category_id'];

$a1 = mysql_query("select * from rooms where category_id={$categoryId} and status=1");
$nr = mysql_num_rows($a1);

$rooms = [];
$num = 1;

while ($p = mysql_fetch_array($a1)) {
    $rooms[] = ['room_no' => $p['room_number'], 'amount' => $p['amount']];
}
?>
<!DOCTYPE html>
    <?php include '_head.php'; ?>
    <body>
        <div class="container">
            <div class="row">
                <div class="section-header">
                    <div class="span5">
                        <!-- <h3 class="section-title"></h3> -->
                    </div>
                    <div class="span7">

                        <div class="custom sec-menu clearfix">
                            <ul class="pull-right clearfix">
                                <li><a href="index.php"><i class="icon-arrow-left icon-x back"></i></a></li>
                                <li><a href="#home"><i class="icon-home icon-x back"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" ng-app="hotel" ng-controller="hotelCtrl">
                <div class="span12">

                    <div class="ray-m-top">
                        <div class="ray-m-top-left"> <? echo date("D, F j, Y."); ?></div>
                        <div class="ray-m-top-right"> <? echo date("G:i:s a"); ?></div>
                        <div class="clear-ray"></div>
                    </div>

                    <div class="ray-room-top">

                        <?php
                        $a1 = mysql_query("select r.*, rc.price from rooms r INNER JOIN rooms_category rc ON rc.category_id = r.category_id where rc.category_id= {$categoryId} and r.status=1");
                        $nr = mysql_num_rows($a1);
                        while ($p = mysql_fetch_array($a1)) :
                            ?>

                            <div class="door" ng-click="showdetails('<?= $p['room_number'] ?>', '<?= $p['price'] ?>')"> <?= $p['room_number'] ?></div>

                        <?php endwhile;
                        ?>
                    </div>
                    <div class="ray-room-note">Touch Desired Room to Take Virtual Tour</div>
                    <div class="row">
                        <div class="span4">
                            <div class="about-service ">

                                <div class="videoBoxLeft">
                                    <div style="height:60px; padding-top:60px; font-weight:bold; font-size:28px; color:black" id="room_num2">
                                        Room {{data.current.room_no}}
                                    </div>
                                    <div style="background-color:#974710; color:white; line-height:35px; height:40px">

                                        <div style="float:left; width:50%" id="price2">{{data.current.amount| currency:'NGN'}}</div>
                                        <div style="float:right; width:50%"><?php echo $nr . " Unit(s) Left" ?></div>
                                        <div class="clear"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="span4Ray">
                            <div class="about-service" >

                                <div class="videoBox">
                                    <video width='100%; height:200px' controls autoplay muted>
                                        <source src="" type='video/mp4'>
                                        <source src="" type='video/ogg'>
                                        Your browser does not support HTML5 video.
                                    </video>
                                    <div id="mydiv"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </body>
</html>