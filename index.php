<?php
error_reporting(0);

include 'connection.php';
conT();

//Select Rooms for Category1
$check = mysql_query("select * from rooms where room_category=1");
$_SESSION['res'] = mysql_fetch_array($check);
mysql_num_rows($check);

//Room Category 1 -
$categories = mysql_query("select * from rooms_category");
$types = mysql_fetch_array($check);


if (strstr($_SERVER['REQUEST_URI'], 'localhost')) {
    die();
}
?>
<!doctype html>
<html prefix="og: http://ogp.me/ns#" xml:lang="en-gb" lang="en-gb" >
    <?php include '_head.php'; ?>
    <body  class="logo-type-custom headerstyle-dark font-family-titillium-web font-size-is-default menu-type-dropdownmenu layout-mode-responsive col12" ng-app="hotel" ng-controller="hotelCtrl">
        <header id="rt-top-surround">
            <div id="rt-top" >
                <div class="rt-container">
                    <div class="rt-grid-12 rt-alpha rt-omega">
                        <div class="rt-block _menu menu-list">
                            <div class="module-surround">
                                <div class="module-content">
                                    <nav class="vertical-menu">
                                        <a class="show-menu"><span class="remove back"></span></a>
                                        <h3><a href="#" title="home">Hotel manager</a></h3>
                                        <ul class="nav menu unstyled">
                                            <li class="item-101"><a class=" menu-home" href="#">Home</a></li>
                                            <li class="item-102"><a class="menu-about" href="#">About</a></li></ul>
                                    </nav>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </header>
        <div id="rt-drawer">
            <div class="rt-container">
                <div class="clear"></div>
            </div>
        </div>
        <div id="rt-transition">
            <div id="rt-mainbody-surround">
                <div class="rt-container">

                    <div id="rt-main" class="mb12">
                        <div class="rt-container static">
                            <article class="item-page">


                                <section id="home" class="page-section home-page blue-sky">
                                    <div class="container">
                                        <div class="row">
                                            <div class="section-header">
                                                <div class="span5">
                                                    <h1 class="site-name">
                                                        <!-- Change HomePage Logo Here -->
                                                        <a href="#"><img src="images/logo.png"></a></h1>
                                                </div>
                                                <div class="span7 menu-button">

                                                    <div class="custom sec-menu clearfix"  >
                                                        <ul class="pull-right clearfix">
                                                            <li><a class="show-menu" title="Display the Right Menu"><span class="icon-reorder icon-x back"></span></a></li>
                                                            <!-- Display The Menu -->
                                                        </ul></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="span4"><!-- column1 -->
                                                <div class="row">
                                                    <div class="span4">
                                                        <div class="latest-project box dark-purple">

                                                            <div id="k2ModuleBox134" class="k2ItemsBlock">


                                                                <ul>
                                                                    <li class="even lastItem">
                                                                        <aside class="item-info">
                                                                            <div class="avatar-pic">
                                                                            </div>
                                                                        </aside>
                                                                        <!-- K2 Plugins: K2BeforeDisplayContent -->

                                                                        <div class="moduleItemIntrotext">
                                                                            <div class="moduleItemIntroImage">
                                                                                <a class="moduleItemImage" href="#" title="UniHOLD">
                                                                                    <img src="images/f7a0a54c92471ac4480e727e4ccf93df_XL.jpg" alt="HotelManager"/>
                                                                                </a>
                                                                            </div>

                                                                            <a class="moduleItemTitle" href="#">Welcome to Castle Hotel & Suites</a>

                                                                        </div>


                                                                        <div class="clr"></div>


                                                                        <div class="clr"></div>

                                                                        <span class="category-name">Castle : <a class="moduleItemCategory" href="#">home away from home</a></span>


                                                                        <div class="clr"></div>
                                                                    </li>
                                                                    <li class="clearList"></li>
                                                                </ul>



                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="span4">

                                                        <div class="custom rotator"  >
                                                            <div class="testimonials box red">
                                                                <h3 class="box-title">Home away from Home</h3>
                                                                <ul class="unstyled inline-block">
                                                                    <li>
                                                                        <blockquote>
                                                                            <p style="font-size:22px">Touch the room category to see features and availability.</p>
                                                                    </li>
                                                                    <li>
                                                                        <blockquote>
                                                                            <p style="font-size:22px">Touch the room category to see features and availability.</p>

                                                                    </li>

                                                                </ul>
                                                            </div></div>
                                                    </div>
                                                    <div class="span2">

                                                        <div class="custom"  >
                                                            <div class="contact box belize-hole"><a href="#" title="contact">
                                                                    <h3 class="box-title">contact</h3>
                                                                    <span class="icon-envelope-alt icon-4x"></span> </a></div></div>
                                                    </div>
                                                    <div class="span2">


                                                    </div>
                                                </div>
                                            </div>
                                            <!-- column1 -->
                                            <div class="span8"><!-- column2 -->
                                                <div class="row">
                                                    <div class="span4">


                                                    </div>

                                                    <!-- 1st Category Box Starts here - you can remove entire section to delete category-->
                                                    <?php while ($row_result = mysql_fetch_array($categories)): ?>
                                                        <div class="span2">
                                                            <div class="custom"  >
                                                                <div class="<?php echo $row_result['category_id'] % 2 == 0 ? 'facebook box facebook-blue' : 'about box orange' ?>">
                                                                    <a href="category.php?category_id=<?= $row_result['category_id'] ?>">
                                                                        <h2 class="box-title" style="margin-top:40px"> <?php echo $row_result['category_name']; ?> <br> <span style="font-size:16px;">
                                                                                <?php echo "N" . number_format($row_result['price'], 2); ?> </span></h2><span class="icon-home icon-5x"></span> 
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endwhile ?>
                                                    <!-- Box  End-->

                                                </div>
                                            </div>
                                            <!-- column3 --></div>
                                    </div>
                                </section>

                                <!-- Second Room Category Section Starts Here-->
                                <section id="about2" class="page-section about2-page carrot">
                                    <!-- Page Content For Category  Starts Here-->
                                    <div class="container">
                                        <div class="row">
                                            <div class="section-header">
                                                <div class="span5">
                                                    <!-- <h3 class="section-title"></h3> -->
                                                </div>
                                                <div class="span7">

                                                    <div class="custom sec-menu clearfix">
                                                        <ul class="pull-right clearfix">
                                                            <li><i class="icon-arrow-left icon-x back" onClick="window.history.back()"></i></li>
                                                            <li><a href="#home"><i class="icon-home icon-x back"></i></a></li>
                                                            <!--Accessing the search field--></ul></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="span12">

                                                <div class="ray-m-top">
                                                    <div class="ray-m-top-left"> <? echo date("D, F j, Y."); ?></div>
                                                    <div class="ray-m-top-right"> <? echo date("G:i:s a"); ?></div>
                                                    <div class="clear-ray"></div>
                                                </div>

                                                <div class="ray-room-top">

                                                    <div ng-repeat="room in data.rooms" class="door" ng-click="showdetails(room)">{{room.room_no}}</div>

                                                </div>
                                                <div class="ray-room-note">Touch Desired Room to Take Virtual Tour</div>
                                                <div class="row">
                                                    <div class="span4">
                                                        <div class="about-service ">

                                                            <div class="videoBoxLeft">
                                                                <div style="height:60px; padding-top:60px; font-weight:bold; font-size:28px; color:black" id="room_num2">
                                                                    Room Number {{data.current.room_no}}
                                                                </div>
                                                                <div style="background-color:#974710; color:white; line-height:35px; height:40px">

                                                                    <div style="float:left; width:50%" id="price2">{{data.current.amount| currency:'NGN'}}</div>
                                                                    <div style="float:right; width:50%">{{data.units_left}} Unit(s) Left</div>
                                                                    <div class="clear"></div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span4Ray">
                                                        <div class="about-service" >

                                                            <div id="videoBox" class="videoBox">
                                                                <video id="veo_room" width='100%; height:200px' controls>
                                                                    <source src="{{data.videoSourceMp4}}" type='video/mp4'>
                                                                    <source src="{{data.videoSourceOgg}}" type='video/ogg'>
                                                                    Your browser does not support HTML5 video.
                                                                </video>
                                                                <div id="mydiv"></div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </body>
                                    </html>

                                    
                                </section>


                                <!-- 8th Room Category Section Ends Here-->


                                <!-- contact section-->
                                <section id="contact" class="page-section contact-page belize-hole">
                                    <div class="container">
                                        <div class="row">
                                            <div class="section-header">
                                                <div class="span5">
                                                    <h3 class="section-title">Contact us</h3>
                                                </div>
                                                <div class="span7">

                                                    <div class="custom sec-menu clearfix"  >
                                                        <ul class="pull-right clearfix">
                                                            <li><i class="icon-arrow-left icon-x back" onClick="window.history.back()"></i></li>
                                                            <li><a href="#home"><i class="icon-home icon-x back"></i></a></li>
                                                            <li><a class="show-menu" title="Display the Right Menu"><span class="icon-reorder icon-x back"></span></a></li>
                                                            <!-- Display The Menu -->
                                                            <!--Accessing the search field--></ul></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="span12">
                                                <p class="lead">
                                                    Vous pouvez nous contacter lectus. Integer laoreet urna tincidunt sem suscipit, pretium congue nunc dapibus. Proin ac pretium quam. Sed in metus quis elit elementum vestibulum. Quisque tincidunt, ante a egestas venenatis, est neque mattis nisl, ut volutpat turpis erat ac neque.
                                                </p>
                                            </div>
                                            <div class="span12">
                                                <!--{emailcloak=off}-->
                                                <a name="b2jmoduleid_130"></a>

                                                <div
                                                    id="b2jcontainer_m130"
                                                    class="b2jcontainer b2j-contact">


                                                    <form enctype="multipart/form-data"
                                                          id="b2j_form_m130"
                                                          name="b2j_form_m130"
                                                          class="b2j_form b2jform-stacked"
                                                          method="post"
                                                          action="#">
                                                        <div class='b2j_clearfix'></div><div class='b2j-contact-group-class '><div class="control-group"><label class="control-label">Your name<span class="required"></span></label><div class="controls"><input type="text" value="" title="Your name" name="dynamic_0" onFocus="if (this.value == 'admin')
                                                                    this.value = '';" onBlur="if (this.value == '')
                                                                                this.value = 'admin';" /></div></div><div class="control-group"><label class="control-label">You E-mail<span class="required"></span></label><div class="controls"><input type="text" value="" title="You E-mail" name="dynamic_3" onFocus="if (this.value == 'you@yourdomain.com')
                                                                                            this.value = '';" onBlur="if (this.value == '')
                                                                                                        this.value = 'you@yourdomain.com';" /></div></div><div class="control-group"><label class="control-label">subject<span class="required"></span></label><div class="controls"><input type="text" value="" title="subject" name="dynamic_2" /></div></div><div class="control-group"><label class="control-label">Message<span class="required"></span></label><div class="controls"><textarea rows="10" cols="30" name="dynamic_4" title="Message" ></textarea></div></div><div class="control-group"><div class="controls"><label class="checkbox"><input type="checkbox" value="Yes" name="dynamic_5" id="dynamic_c5" />Send yourself  a copy</label></div></div><div class='b2j_clearfix'>div></div><div class="control-group"><label class="control-label"></label><div class="controls"><div>.</div></div></div><div class="control-group b2j-contact-actions"><div class="controls">
                                                                    <button class="btn btn-success" type="submit" style="margin-right:32px;" name="mid_130">
                                                                        <span style="background: url(images/submit.png) no-repeat left center transparent; padding-left:20px;" >
                                                                            Submit</span>
                                                                    </button>
                                                                </div></div><div class="b2j_clearfix"></div>	</form>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </section>
                                <!-- contact section-->
                            </article>

                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include '_price_modal.php'; ?>
    </body>
</html>
