<?php
session_start();
error_reporting(0);

$staff = $_SESSION['admin']['name'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Hotel Manager Software</title>
<link type="text/css" rel="stylesheet" href="style.css" />
</head>

<body>
<div class="header">
  <div class="logo">
		<h1>Hotel Manager</h1>

	</div>
</div>
    <div style="clear:both"></div>
<div class="menu">
<div style="height:10px;"></div>
<div style="float:left; margin-left:10px; font-family:Arial; font-size:13px; ">welcome: <font style="color:white"><?php echo $_SESSION['admin']['name'];?></font></div>

<div style="float:right; width:100px; margin-right:20px; font-family:Arial; font-size:14px"><a href="logout.php"><img src="images/logout.png" width="87" height="24" /></a></div>
</div>
<div class="container">
	<div class="container_left">
	<div class="messaging">
	<div style="background-color:#CECF81; height:30px; font-family:Arial, Helvetica, sans-serif; font-size:17px; line-height:30px; font-weight:bold
">&nbsp;&nbsp;  ADMIN SETUP</div>
<div class="list">
<ul>

<li><a href="home.php?page=admin">Add Admin</a></li>
<li><a href="home.php?page=changePass">Change Password</a></li>
</ul>
</div>

<div style="background-color:#CECF81; height:30px; font-family:Arial, Helvetica, sans-serif; font-size:17px; line-height:30px; font-weight:bold
">&nbsp;&nbsp; MANAGE ROOMS </div>
    <div class="list">
      <ul>
      	<li><a href="home.php?page=addcat">Setup Category</a></li>
        <li><a href="home.php?page=cat_dir">Category Directory</a></li>
        <li><a href="home.php?page=adroom">Setup Rooms</a></li>
        <li><a href="home.php?page=rooms_dir">Rooms Directory</a></li>
      </ul>
    </div>
	</div>
	</div>
<div class="container_right">
<?php
if($_GET["page"]=="crown_suite")
include("crown-suite.php");
else
if($_GET["page"]=="executive_suite")
include("executive-suite.php");
else
if($_GET["page"]=="queen_suite")
include("queen-suite.php");
else
if($_GET["page"]=="crown_deluxe")
include("crown-deluxe.php");
else
if($_GET["page"]=="royal_deluxe")
include("royal-deluxe.php");
else
if($_GET["page"]=="changePass")
include("changepass.php");
else
if($_GET["page"]=="rooms_dir")
include("rooms_directory.php");
else
if($_GET["page"]=="rooms-profile")
include("rooms-profile.php");
else
if($_GET["page"]=="category-profile")
include("category-profile.php");
else
if($_GET["page"]=="admin")
include("add_admin.php");
else
if($_GET["page"]=="adroom")
include("add_rooms.php");
else
if($_GET["page"]=="addcat")
include("add_cat.php");
else
if($_GET["page"]=="cat_dir")
include("cat_directory.php");
else
include("intro.php");
 ?>
</div>
<div class="clear"></div>
</div>
</body>
</html>
