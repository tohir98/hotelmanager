<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Super User" />
    <meta name="generator" content="" />
    <title>Royal Castle Hotel</title>
    <link rel="shortcut icon" href="images/fav.fw.png" />
    <link rel="stylesheet" href="stylesheets/k2.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/style.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/grid-responsive.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/master-d26a56eb8cdc90f01bdcd6f9ae017ece.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/mediaqueries.css" type="text/css" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Titillium+Web&subset=latin,latin-ext" type="text/css" />
    <link rel="stylesheet" href="stylesheets/flatmetro-colors.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/flatmetro.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/animations.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/animate.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/linecons.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/showcase.css" type="text/css" />
    <link rel="stylesheet" href="stylesheets/mosaic.css" type="text/css" />
    <style type="text/css">

        h1, h2 { font-family: 'Titillium Web', 'Helvetica', arial, serif; }
    </style>
    <script src="scripts/jquery.min.js" type="text/javascript"></script>
    <script src="scripts/mootools-core.js" type="text/javascript"></script>
    <script src="scripts/core.js" type="text/javascript"></script>

    <script src="scripts/jquery-noconflict.js" type="text/javascript"></script>
    <script src="scripts/jquery-migrate.min.js" type="text/javascript"></script>
    <!--<script src="Scripts/flatmetro/" type="text/javascript"></script>-->
    <script src="scripts/tabs-state.js" type="text/javascript"></script>
    <script src="scripts/behaviour.js?v=2.1" type="text/javascript"></script>
    <script src="scripts/mootools-more.js" type="text/javascript"></script>
    <script src="scripts/fileuploader.js" type="text/javascript"></script>
    <script src="scripts/chosen.jquery.min.js" type="text/javascript"></script>
    <script src="scripts/glDatePicker.js" type="text/javascript"></script>
    <script src="scripts/browser-engines.js" type="text/javascript"></script>
    <script src="scripts/rokmediaqueries.js" type="text/javascript"></script>
    <script src="scripts/load-transition.js" type="text/javascript"></script>
    <script src="scripts/modernizr.custom.js" type="text/javascript"></script>
    <script src="scripts/quovolver.js" type="text/javascript"></script>
    <script src="scripts/pagetransitions.js" type="text/javascript"></script>
    <script src="scripts/flatmetro.js" type="text/javascript"></script>
    <script src="scripts/mootools-mobile.js" type="text/javascript"></script>
    <script src="scripts/rokmediaqueries.js" type="text/javascript"></script>
    <script src="scripts/roksprocket.js" type="text/javascript"></script>
    <script src="scripts/moofx.js" type="text/javascript"></script>
    <script src="scripts/features.js" type="text/javascript"></script>
    <script src="scripts/showcase.js" type="text/javascript"></script>
    <script src="scripts/roksprocket.request.js" type="text/javascript"></script>
    <script src="scripts/mosaic.js" type="text/javascript"></script>
    <script type="text/javascript">

        animcursor = 1


        window.addEvent('domready', function () {
            RokSprocket.instances.showcase = new RokSprocket.Showcase();
        });

        window.addEvent('domready', function () {
            RokSprocket.instances.showcase.attach(127, '{"animation":"crossfade","autoplay":"1","delay":"5"}');
        });

        window.addEvent('domready', function () {
            RokSprocket.instances.mosaic = new RokSprocket.Mosaic();
        });

        window.addEvent('domready', function () {
            RokSprocket.instances.mosaic.attach(115, '{"pages":1,"animations":["fade","scale","rotate","flip"],"displayed":[8,9,10,11,12,13,14,15,16]}');
        });

    </script>
    <script src="scripts/angular.min.js" type="text/javascript"></script>
    <script src="scripts/angular-busy.js" type="text/javascript"></script>
    <script src="scripts/hotel.js" type="text/javascript"></script>
</head>
