var hotelApp = angular.module('hotel', ['cgBusy']);

var getSiteUrl = function () {
    return window.location.protocol + '//' + window.location.host + '/';
};

hotelApp.controller('hotelCtrl', function ($rootScope, $scope, $http) {
    $scope.data = {rooms: [], current: [], units_left: 0, videoSourceMp4:'', videoSourceOgg:'', videoSourceWebm:''};

    $scope.init = function () {
        $scope.data = {rooms: [], current: [], units_left: 0};
    };

    $scope.showdetails = function (room_no, amount) {
        
        $scope.data.current.room_no = room_no;
        $scope.data.current.amount = amount;
//        $scope.data.videoSourceMp4 = 'video/' + room_no + '.mp4';
//        $scope.data.videoSourceOgg = 'video/' + room_no + '.ogg';
//        $scope.data.videoSourceWebm = 'video/' + room_no + '.webm';
        
        var video = document.getElementsByTagName('video')[0];
        var sources = video.getElementsByTagName('source');
                                                                                sources[0].setAttribute('src', 'video/' + room_no + '.mp4');
                                                                                sources[1].setAttribute('src', 'video/' + room_no + '.ogg');
        video.load();

    };

    $scope.showModal = function (id_) {
        $scope.init();

        $scope.roomsPromise = $http.get(getSiteUrl() + 'fetchCategoryRooms.php?category_id=' + id_)
                .success(function (data) {
                    $scope.data.rooms = data.rooms;
                    $scope.data.units_left = data.units_left;
                });
        window.location = getSiteUrl() + "#about2";
    };
});