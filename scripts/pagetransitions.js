(function() {
	jQuery(function() {//On Document Ready 
	var Modernizr = window.Modernizr, 
		$body = jQuery( 'body' ),
		$main = jQuery( '.item-page' ),
		$pages = $main.children( 'section.page-section' ),
		$iterate = jQuery( '#iterateEffects' ),
		pagesCount = $pages.length,
		current = 0,
		isAnimating = false,
		endCurrPage = false,
		endNextPage = false,
		animEndEventNames = {
			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
		},
		// animation end event name
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		// support css animations
		support = Modernizr.cssanimations;
	function locationHashChanged() {
	    var pagesClasses = [];
	    jQuery.each( $pages, function(ind, itm){
	        var classList = itm.className.split(/\s+/);
	        var sectionclass = classList[1].replace('-page','');
	        pagesClasses.push( sectionclass );
	    });

	    var hash = 'home';
	    if(window.location.hash) {
	        hash = window.location.hash.substring(1);
	    }

	    var hashIndex = jQuery.inArray( hash, pagesClasses );
	    if( hashIndex !== -1 ) {
	        nextPage( animcursor, hashIndex );
	    }
	}
	setTimeout(function() {
	  if(window.location.hash && window.location.hash != '#home') {
	    locationHashChanged();
	  }
	 }, 1500);

	window.onhashchange = locationHashChanged;	
	function init() {

		$pages.each( function() {
			var $page = jQuery( this );
			$page.data( 'originalClassList', $page.attr( 'class' ) );
		} );

		$pages.eq( current ).addClass( 'page-section-current' );

		$iterate.on( 'click', function() {
			if( isAnimating ) {
				return false;
			}
			if( animcursor > 67 ) {
				animcursor = 1;
			}
			nextPage( animcursor );
			++animcursor;
		} );

	}

	function nextPage( animation, cur ) {

		if( isAnimating ) {
		  return false;
		}

		isAnimating = true;
		
		var $currPage = $pages.eq( current );
		
		if(typeof cur == 'undefined'){
		  current = 0;
		}else{
		  current = cur;
		}

		var $nextPage = $pages.eq( current ).addClass( 'page-section-current' ),
			outClass = '', inClass = '';

		switch( animation ) {

			case 1:
				outClass = 'page-section-moveToLeft';
				inClass = 'page-section-moveFromRight';
				break;
			case 2:
				outClass = 'page-section-moveToRight';
				inClass = 'page-section-moveFromLeft';
				break;
			case 3:
				outClass = 'page-section-moveToTop';
				inClass = 'page-section-moveFromBottom';
				break;
			case 4:
				outClass = 'page-section-moveToBottom';
				inClass = 'page-section-moveFromTop';
				break;
			case 5:
				outClass = 'page-section-fade';
				inClass = 'page-section-moveFromRight page-section-ontop';
				break;
			case 6:
				outClass = 'page-section-fade';
				inClass = 'page-section-moveFromLeft page-section-ontop';
				break;
			case 7:
				outClass = 'page-section-fade';
				inClass = 'page-section-moveFromBottom page-section-ontop';
				break;
			case 8:
				outClass = 'page-section-fade';
				inClass = 'page-section-moveFromTop page-section-ontop';
				break;
			case 9:
				outClass = 'page-section-moveToLeftFade';
				inClass = 'page-section-moveFromRightFade';
				break;
			case 10:
				outClass = 'page-section-moveToRightFade';
				inClass = 'page-section-moveFromLeftFade';
				break;
			case 11:
				outClass = 'page-section-moveToTopFade';
				inClass = 'page-section-moveFromBottomFade';
				break;
			case 12:
				outClass = 'page-section-moveToBottomFade';
				inClass = 'page-section-moveFromTopFade';
				break;
			case 13:
				outClass = 'page-section-moveToLeftEasing page-section-ontop';
				inClass = 'page-section-moveFromRight';
				break;
			case 14:
				outClass = 'page-section-moveToRightEasing page-section-ontop';
				inClass = 'page-section-moveFromLeft';
				break;
			case 15:
				outClass = 'page-section-moveToTopEasing page-section-ontop';
				inClass = 'page-section-moveFromBottom';
				break;
			case 16:
				outClass = 'page-section-moveToBottomEasing page-section-ontop';
				inClass = 'page-section-moveFromTop';
				break;
			case 17:
				outClass = 'page-section-scaleDown';
				inClass = 'page-section-moveFromRight page-section-ontop';
				break;
			case 18:
				outClass = 'page-section-scaleDown';
				inClass = 'page-section-moveFromLeft page-section-ontop';
				break;
			case 19:
				outClass = 'page-section-scaleDown';
				inClass = 'page-section-moveFromBottom page-section-ontop';
				break;
			case 20:
				outClass = 'page-section-scaleDown';
				inClass = 'page-section-moveFromTop page-section-ontop';
				break;
			case 21:
				outClass = 'page-section-scaleDown';
				inClass = 'page-section-scaleUpDown page-section-delay300';
				break;
			case 22:
				outClass = 'page-section-scaleDownUp';
				inClass = 'page-section-scaleUp page-section-delay300';
				break;
			case 23:
				outClass = 'page-section-moveToLeft page-section-ontop';
				inClass = 'page-section-scaleUp';
				break;
			case 24:
				outClass = 'page-section-moveToRight page-section-ontop';
				inClass = 'page-section-scaleUp';
				break;
			case 25:
				outClass = 'page-section-moveToTop page-section-ontop';
				inClass = 'page-section-scaleUp';
				break;
			case 26:
				outClass = 'page-section-moveToBottom page-section-ontop';
				inClass = 'page-section-scaleUp';
				break;
			case 27:
				outClass = 'page-section-scaleDownCenter';
				inClass = 'page-section-scaleUpCenter page-section-delay400';
				break;
			case 28:
				outClass = 'page-section-rotateRightSideFirst';
				inClass = 'page-section-moveFromRight page-section-delay200 page-section-ontop';
				break;
			case 29:
				outClass = 'page-section-rotateLeftSideFirst';
				inClass = 'page-section-moveFromLeft page-section-delay200 page-section-ontop';
				break;
			case 30:
				outClass = 'page-section-rotateTopSideFirst';
				inClass = 'page-section-moveFromTop page-section-delay200 page-section-ontop';
				break;
			case 31:
				outClass = 'page-section-rotateBottomSideFirst';
				inClass = 'page-section-moveFromBottom page-section-delay200 page-section-ontop';
				break;
			case 32:
				outClass = 'page-section-flipOutRight';
				inClass = 'page-section-flipInLeft page-section-delay500';
				break;
			case 33:
				outClass = 'page-section-flipOutLeft';
				inClass = 'page-section-flipInRight page-section-delay500';
				break;
			case 34:
				outClass = 'page-section-flipOutTop';
				inClass = 'page-section-flipInBottom page-section-delay500';
				break;
			case 35:
				outClass = 'page-section-flipOutBottom';
				inClass = 'page-section-flipInTop page-section-delay500';
				break;
			case 36:
				outClass = 'page-section-rotateFall page-section-ontop';
				inClass = 'page-section-scaleUp';
				break;
			case 37:
				outClass = 'page-section-rotateOutNewspaper';
				inClass = 'page-section-rotateInNewspaper page-section-delay500';
				break;
			case 38:
				outClass = 'page-section-rotatePushLeft';
				inClass = 'page-section-moveFromRight';
				break;
			case 39:
				outClass = 'page-section-rotatePushRight';
				inClass = 'page-section-moveFromLeft';
				break;
			case 40:
				outClass = 'page-section-rotatePushTop';
				inClass = 'page-section-moveFromBottom';
				break;
			case 41:
				outClass = 'page-section-rotatePushBottom';
				inClass = 'page-section-moveFromTop';
				break;
			case 42:
				outClass = 'page-section-rotatePushLeft';
				inClass = 'page-section-rotatePullRight page-section-delay180';
				break;
			case 43:
				outClass = 'page-section-rotatePushRight';
				inClass = 'page-section-rotatePullLeft page-section-delay180';
				break;
			case 44:
				outClass = 'page-section-rotatePushTop';
				inClass = 'page-section-rotatePullBottom page-section-delay180';
				break;
			case 45:
				outClass = 'page-section-rotatePushBottom';
				inClass = 'page-section-rotatePullTop page-section-delay180';
				break;
			case 46:
				outClass = 'page-section-rotateFoldLeft';
				inClass = 'page-section-moveFromRightFade';
				break;
			case 47:
				outClass = 'page-section-rotateFoldRight';
				inClass = 'page-section-moveFromLeftFade';
				break;
			case 48:
				outClass = 'page-section-rotateFoldTop';
				inClass = 'page-section-moveFromBottomFade';
				break;
			case 49:
				outClass = 'page-section-rotateFoldBottom';
				inClass = 'page-section-moveFromTopFade';
				break;
			case 50:
				outClass = 'page-section-moveToRightFade';
				inClass = 'page-section-rotateUnfoldLeft';
				break;
			case 51:
				outClass = 'page-section-moveToLeftFade';
				inClass = 'page-section-rotateUnfoldRight';
				break;
			case 52:
				outClass = 'page-section-moveToBottomFade';
				inClass = 'page-section-rotateUnfoldTop';
				break;
			case 53:
				outClass = 'page-section-moveToTopFade';
				inClass = 'page-section-rotateUnfoldBottom';
				break;
			case 54:
				outClass = 'page-section-rotateRoomLeftOut page-section-ontop';
				inClass = 'page-section-rotateRoomLeftIn';
				break;
			case 55:
				outClass = 'page-section-rotateRoomRightOut page-section-ontop';
				inClass = 'page-section-rotateRoomRightIn';
				break;
			case 56:
				outClass = 'page-section-rotateRoomTopOut page-section-ontop';
				inClass = 'page-section-rotateRoomTopIn';
				break;
			case 57:
				outClass = 'page-section-rotateRoomBottomOut page-section-ontop';
				inClass = 'page-section-rotateRoomBottomIn';
				break;
			case 58:
				outClass = 'page-section-rotateCubeLeftOut page-section-ontop';
				inClass = 'page-section-rotateCubeLeftIn';
				break;
			case 59:
				outClass = 'page-section-rotateCubeRightOut page-section-ontop';
				inClass = 'page-section-rotateCubeRightIn';
				break;
			case 60:
				outClass = 'page-section-rotateCubeTopOut page-section-ontop';
				inClass = 'page-section-rotateCubeTopIn';
				break;
			case 61:
				outClass = 'page-section-rotateCubeBottomOut page-section-ontop';
				inClass = 'page-section-rotateCubeBottomIn';
				break;
			case 62:
				outClass = 'page-section-rotateCarouselLeftOut page-section-ontop';
				inClass = 'page-section-rotateCarouselLeftIn';
				break;
			case 63:
				outClass = 'page-section-rotateCarouselRightOut page-section-ontop';
				inClass = 'page-section-rotateCarouselRightIn';
				break;
			case 64:
				outClass = 'page-section-rotateCarouselTopOut page-section-ontop';
				inClass = 'page-section-rotateCarouselTopIn';
				break;
			case 65:
				outClass = 'page-section-rotateCarouselBottomOut page-section-ontop';
				inClass = 'page-section-rotateCarouselBottomIn';
				break;
			case 66:
				outClass = 'page-section-rotateSidesOut';
				inClass = 'page-section-rotateSidesIn page-section-delay200';
				break;
			case 67:
				outClass = 'page-section-rotateSlideOut';
				inClass = 'page-section-rotateSlideIn';
				break;

		}

		$currPage.addClass( outClass ).on( animEndEventName, function() {
			$currPage.off( animEndEventName );
			endCurrPage = true;
			if( endNextPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		$nextPage.addClass( inClass ).on( animEndEventName, function() {
			$nextPage.off( animEndEventName );
			endNextPage = true;
			if( endCurrPage ) {
				onEndAnimation( $currPage, $nextPage );
			}
		} );

		if( !support ) {
			onEndAnimation( $currPage, $nextPage );
		}

	}

	function onEndAnimation( $outpage, $inpage ) {
		endCurrPage = false;
		endNextPage = false;
		resetPage( $outpage, $inpage );
		isAnimating = false;
	}

	function resetPage( $outpage, $inpage ) {
		$outpage.attr( 'class', $outpage.data( 'originalClassList' ) );
		$inpage.attr( 'class', $inpage.data( 'originalClassList' ) + ' page-section-current' );
	}

	init();

	return { init : init };
});//end document ready function
})();