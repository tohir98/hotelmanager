/* playing with menu*/
(function() {
	jQuery(function() {
		jQuery('.vertical-menu').animate({width: [ "toggle", "linear" ] }, 80, "linear"); 

		jQuery('.show-menu').on( 'click', function() { 
		  jQuery('.vertical-menu').animate({width: [ "toggle", "linear" ] }, 80, "linear");
		  
		  if( jQuery(this).hasClass('search') ){
		    jQuery('.vertical-menu .inputbox').focus();

		  }    
		});

		jQuery('#mod-search-searchword').addClass('span3');
		
		//Injecting custom icons to menu
		jQuery('.menu-home').prepend('<span class="icon-home icon-x red"></span>');		
		jQuery('.menu-about').prepend('<span class="icon-comment-alt icon-x orange"></span>');		
		jQuery('.menu-team').prepend('<span class="icon-group icon-x red"></span>');		
		jQuery('.menu-services').prepend('<span class="icon-puzzle-piece icon-x blue-sky"></span>');		
		jQuery('.menu-portfolio').prepend('<span class="icon-briefcase icon-x teal"></span>');		
		jQuery('.menu-blog').prepend('<span class="icon-edit icon-x blue"></span>');		
		jQuery('.menu-contact').prepend('<span class="icon-envelope-alt icon-x purple"></span>');
		
		//tertiary nav
		jQuery('.tertiary-home').prepend('<span class="icon-home"></span>');		
		jQuery('.tertiary-blog').prepend('<span class="icon-edit"></span>');		

		//Quotes Revolver caller
		jQuery('.rotator blockquote').quovolver();

		//portfolio adding removing classes

		jQuery('.sprocket-padding').hover(function() {
		      var jQuerythis = jQuery(this);
		      if(Modernizr.csstransitions) {
		          jQuery('.plus-icon', jQuerythis).addClass('animated');
		          jQuery('.plus-icon', jQuerythis).css('display', 'block');
		          jQuery('.plus-icon', jQuerythis).removeClass('flipOutX'); 
		          jQuery('.plus-icon', jQuerythis).addClass('flip'); 
		          
		          jQuery('.sprocket-mosaic-head h3', jQuerythis).addClass('animated');
		          jQuery('.sprocket-mosaic-head h3', jQuerythis).removeClass('fadeOutUp'); 
		          jQuery('.sprocket-mosaic-head h3', jQuerythis).addClass('fadeInDown'); 
		      }else{
		          jQuery('.plus-icon', jQuerythis).stop(true, false).fadeIn('fast');
		      }
		  }, function() {
		      var jQuerythis = jQuery(this);
		      if(Modernizr.csstransitions) {
		          jQuery('.plus-icon', jQuerythis).removeClass('flip'); 
		          jQuery('.plus-icon', jQuerythis).addClass('flipOutX');
		          jQuery('.plus-icon', jQuerythis).css('display', 'none');
		          jQuery('.plus-icon', jQuerythis).removeClass('animated');
		          
		          jQuery('.sprocket-mosaic-head h3', jQuerythis).removeClass('fadeInDown'); 
		          jQuery('.sprocket-mosaic-head h3', jQuerythis).addClass('fadeOutUp'); 
		          jQuery('.plus-icon', jQuerythis).removeClass('animated');
		      }else{
		          jQuery('.plus-icon', jQuerythis).stop(true, false).fadeOut('fast');
		      }
		  });

	});
})();